<?php
class Templateback{
    protected $_ci;
    
    function __construct(){
        $this->_ci = &get_instance();
    }
    
  function utama($content, $data = NULL){
    /*
     * $data['headernya'] , $data['contentnya'] , $data['footernya']
     * variabel diatas ^ nantinya akan dikirim ke file views/template/index.php
     * */
        $data['headerback'] = $this->_ci->load->view('backend/template/header', $data, TRUE);
        $data['contenback'] = $this->_ci->load->view($content, $data, TRUE);
        $data['footerback'] = $this->_ci->load->view('backend/template/footer', $data, TRUE);
        
        $this->_ci->load->view('backend/template/index', $data);
    }
}