 <div class="navbar navbar-fixed-top">
			<div class="container">
				<div class="row navbar-skala">
					<div class="col-md-3">
						<a href="#"><img src="http://sukodono.id/com/upload/images/Kecamatan%20Sukodono%201.png" class="img-responsive" style="max-height: 100px;"></a>
					</div>
					<!--menu-->
					<div class="col-md-9">
						<nav class="navbar navbar-default navbar-static-top">
							<div class="">

								<div class="collapse navbar-collapse" id="navbar">
									<ul class="nav navbar-nav navbar-left">
										<li><a href="http://sukodono.id/"><i class="fa fa-home fa-fw"></i> <span>HOME</span></a></li>
										<li class="dropdown kotak-0">
									<a href="http://sukodono.id/#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-question-sign fa-fw"></i> <span>PROFIL<span> <span class="caret"></span></span></span></a>
										<ul class="dropdown-menu  kotak-0"><li><a href="http://sukodono.id/page/33/sambutan-camat"><span>SAMBUTAN CAMAT</span></a><span></span></li><li><a href="http://sukodono.id/page/34/selayang-pandang"><span>SELAYANG PANDANG</span></a><span></span></li><li><a href="http://sukodono.id/page/35/visi-dan-misi"><span>VISI &amp; MISI</span></a><span></span></li><li class="dropdown-submenu kotak-0">
											<a href="http://sukodono.id/#"><span>ORGANISASI</span></a>
												<ul class="dropdown-menu kotak-0"><li><a href="http://sukodono.id/page/36/struktur-organisasi" class="dropdown-1">STRUKTUR ORGANISASI</a></li><li><a href="http://sukodono.id/page/37/camat-sukodono" class="dropdown-1">CAMAT SUKODONO</a></li><li><a href="http://sukodono.id/page/38/aparatur-kecamatan" class="dropdown-1">APARATUR KECAMTAN</a></li></ul></li><li><a href="http://sukodono.id/page/39/peta-dan-batas-wilayah"><span>PETA DAN BATAS WILAYAH</span></a><span></span></li><li><a href="http://sukodono.id/page/40/program-unggulan"><span>PROGRAM UNGGULAN</span></a><span></span></li><li><a href="http://sukodono.id/page/41/moto-pelayanan"><span>MOTO PELAYANAN</span></a><span></span></li><li><a href="http://sukodono.id/page/42/prestasi-kecamatan"><span>PRESTASI KECAMATAN</span></a><span></span></li></ul></li><li class="dropdown kotak-1">
									<a href="http://sukodono.id/#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-map-marker fa-fw"></i> <span>DESA<span> <span class="caret"></span></span></span></a>
										<ul class="dropdown-menu  kotak-1"><li><a href="http://suruh.sukodono.id/"><span>SURUH</span></a><span></span></li><li><a href="http://kebonagung.sukodono.id/"><span>KEBONAGUNG</span></a><span></span></li><li><a href="http://anggaswangi.sukodono.id/"><span>ANGGASWANGI</span></a><span></span></li><li><a href="http://jumputrejo.sukodono.id/"><span>JUMPUTREJO</span></a><span></span></li><li><a href="http://wilayut.sukodono.id/"><span>WILAYUT</span></a><span></span></li><li><a href="http://pekarungan.sukodono.id/"><span>PEKARUNGAN</span></a><span></span></li><li><a href="http://pademonegoro.sukodono.id/"><span>PADEMONEGORO</span></a><span></span></li><li><a href="http://cangkringsari.sukodono.id/"><span>CANGKRINGSARI</span></a><span></span></li><li><a href="http://jogosatru.sukodono.id/"><span>JOGOSATRU</span></a><span></span></li><li><a href="http://ngaresrejo.sukodono.id/"><span>NGARESREJO</span></a><span></span></li><li><a href="http://sambungrejo.sukodono.id/"><span>SAMBUNGREJO</span></a><span></span></li><li><a href="http://plumbungan.sukodono.id/"><span>PLUMBUNGAN</span></a><span></span></li><li><a href="http://sukodono.sukodono.id/"><span>SUKODONO</span></a><span></span></li><li><a href="http://keloposepuluh.sukodono.id/"><span>KELOPOSEPULUH</span></a><span></span></li><li><a href="http://masanganwetan.sukodono.id/"><span>MASANGAN WETAN</span></a><span></span></li><li><a href="http://suko.sukodono.id/"><span>SUKO</span></a><span></span></li><li><a href="http://masangankulon.sukodono.id/"><span>MASANGAN KULON</span></a><span></span></li><li><a href="http://panjunan.sukodono.id/"><span>PANJUNAN</span></a><span></span></li><li><a href="http://bangsri.sukodono.id/"><span>BANGSRI</span></a><span></span></li></ul></li><li class="dropdown kotak-2">
									<a href="http://sukodono.id/#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-hdd fa-fw"></i> <span>DATA<span> <span class="caret"></span></span></span></a>
										<ul class="dropdown-menu  kotak-2"><li><a href="http://sukodono.id/page/23/monografi"><span>MONOGRAFI</span></a><span></span></li><li><a href="http://sukodono.id/page/27/kependudukan"><span>KEPENDUDUKAN</span></a><span></span></li><li><a href="http://sukodono.id/page/31/keagamaan"><span>KEAGAMAAN</span></a><span></span></li><li><a href="http://sukodono.id/page/32/pendidikan"><span>PENDIDIKAN</span></a><span></span></li><li><a href="http://sukodono.id/page/28/kesehatan"><span>KESEHATAN</span></a><span></span></li><li><a href="http://sukodono.id/page/29/ekonomi"><span>PEREKONOMIAN</span></a><span></span></li><li><a href="http://sukodono.id/page/30/pertanian"><span>PERTANIAN</span></a><span></span></li><li class="dropdown-submenu kotak-2">
											<a href="http://sukodono.id/#"><span>PEMERINTAHAN</span></a>
												<ul class="dropdown-menu kotak-2"><li><a href="http://sukodono.id/page/46/rencana-strategis" class="dropdown-1">RENCANA STRATEGIS</a></li><li><a href="http://sukodono.id/page/47/rencana-kerja" class="dropdown-1">RENCANA KERJA</a></li><li><a href="http://sukodono.id/page/48/anggaran" class="dropdown-1">ANGGARAN</a></li><li><a href="http://sukodono.id/page/49/sakip" class="dropdown-1">SAKIP</a></li><li><a href="http://sukodono.id/page/50/lakip" class="dropdown-1">LAKIP</a></li></ul></li></ul></li><li class="kotak-3"><a href="http://sukodono.id/web/news"><i class="fa fa-newspaper-o "></i> <span>BERITA</span></a></li><li class="dropdown kotak-4">
									<a href="http://sukodono.id/#" class="dropdown-toggle" data-toggle="dropdown"><i class=" fa-fw"></i> <span>POJOK PKK<span> <span class="caret"></span></span></span></a>
										<ul class="dropdown-menu  kotak-4"><li><a href="http://localhost/webdesa/page/51/organisasi-pkk"><span>ORGANISASI PKK</span></a><span></span></li><li><a href="http://localhost/webdesa/page/52/visi-misi-pkk"><span>VISI MISI PKK</span></a><span></span></li><li><a href="http://localhost/webdesa/page/53/prestasi-pkk"><span>PRESTASI PKK</span></a><span></span></li><li><a href="http://localhost/webdesa/page/54/kegiatan-pkk"><span>KEGIATAN PKK</span></a><span></span></li></ul></li>										<li class="kotak-download"><a href="http://sukodono.id/download"><i class="fa fa-database fa-fw"></i> BANK <span>DATA</span></a></li>
										<li class="kotak-gallery"><a href="http://sukodono.id/gallery"><i class="fa fa-camera fa-fw"></i> <span>GALLERY</span></a></li>
									</ul>
								</div>
								<div class="navbar-header">
									<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
										<span class="sr-only">Toggle navigation</span>
										<span class="icon-bar top-bar"></span>
										<span class="icon-bar middle-bar"></span>
										<span class="icon-bar bottom-bar"></span>
									</button>
								</div>

							</div>
						</nav>
					</div>
					<!--
										<div class="col-md-3 hidden-sm hidden-xs">
						<div class="text-right">
								<div style="width:51px; float:right;"><img src="http://sukodono.id/com/kece/img/icon_location.png"/></div>
								<a class="nav-skala-a" href=""><b>Email</b>,<br/><span>kecamatansukodono30@gmail.com</span></a>
						</div>
					</div>
					<div class="col-md-3 hidden-sm hidden-xs">
						<div class="text-right">
								<div style="width:51px; float:right;"><img src="http://sukodono.id/com/kece/img/icon_contact.png"/></div>
								<a class="nav-skala-a" href=""><b>SMS Pelayanan</b>,<br/><span>0822 4440 1064</span></a>
						</div>
					</div>
											-->
				</div>
			</div>
		</div>