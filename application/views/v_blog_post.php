<!DOCTYPE html>
<html>
<head>
	    <title>Post Artikel</title>
	    <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/style.css') ?>" rel="stylesheet">
	<link href="<?php echo base_url('assets/css/kominfo.css') ?>" rel="stylesheet">
	<link rel="stylesheet" href="<?php echo base_url('assets/css/font-awesome.min.css')?>">
	
</head>
<body>
	    <div class="container">
		        <div class="col-md-8 col-md-offset-2">
			            <h2>MY BLOG</h2><hr/>
			            <form action="<?php echo base_url().'blog/simpan_post'?>" method="post" enctype="multipart/form-data">
				                <input type="text" name="judul" class="form-control" placeholder="Judul" required/><br/>
				                <textarea name="isi" id="editor"></textarea>
				                <input type="file" name="filefoto" required>				
				<button class="btn btn-success" type="submit">POST</button>     
			            </form>
		        </div>                
	    </div>
</body>
<script src="<?php echo ('https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js')?>"></script>
	    <script type="text/javascript" src="<?php echo base_url().'assets/js/bootstrap.min.js'?>"></script>
	   <script src="https://cdn.ckeditor.com/ckeditor5/12.3.1/classic/ckeditor.js"></script>
	<script>
    ClassicEditor
        .create( document.querySelector( '#editor' ) )
        .catch( error => {
            console.error( error );
        } );
</script>
</html>