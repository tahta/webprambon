<div class="col-md-12 blog">	
			<h3 class="text-center">Selayang Pandang </h3>
		<hr>
		<p><strong>Sukodono</strong> adalah sebuah kecamatan di <a href="http://sidoarjokab.go.id">Kabupaten Sidoarjo</a>, Provinsi <a href="http://www.jatimprov.go.id/">Jawa Timur</a>, <a href="http://indonesia.go.id/">Indonesia</a>.</p>

<p>Kantor Camat Sukodono berada di Jl. Raya Sukodono No. 30, Desa Pekarungan, Kecamatan Sukodono, Kabupaten Sidoarjo dengan kode pos 61258.</p>

<p>Wilayah kerja Kantor Kecamatan Sukodono terletak di barat laut Kabupaten Sidoarjo yang berjarak ± 10 km dari kota Sidoarjo dan ± 30 km dari Ibukota Propinsi.</p>

<p>Kecamatan Sukodono merupakan salah satu Kecamatan di Kabupaten Sidoarjo, dengan wilayah yang berbatasan langsung dengan Kota Sidoarjo dan dekat dengan Kota Surabaya menjadikan Kecamatan Sukodono tumbuh dengan sangat pesat. Ini ditandai dengan banyaknya perusahaan yang berani berinvestasi di Kecamatan Sukodono, banyaknya warga terampil yang dapat membuka lapangan kerja baru, hingga peremajaan fasilitas umum seperti jalan, jembatan, Kantor Kecamatan Sukodono dan Puskesmas Sukodono. Karena wilayah Kecamatan Sukodono bebas dari banjir maka kawasan ini sangat nyaman untuk dijadikan hunian</p>

<table border="0" style="height:295px; width:502px">
	<tbody>
		<tr>
			<td>
			<table align="center" border="0" style="width:289px">
				<tbody>
					<tr>
						<td colspan="2">
						<p>Luas Wilayah</p>
						</td>
					</tr>
					<tr>
						<td>
						<p>Tanah Sawah</p>
						</td>
						<td>
						<p>1915 ha</p>
						</td>
					</tr>
					<tr>
						<td>
						<p>Tanah Pekarangan / Bangunan</p>
						</td>
						<td>
						<p>1242 ha</p>
						</td>
					</tr>
					<tr>
						<td>
						<p>Tanah Fasilitas Umum</p>
						</td>
						<td>
						<p>328.74 ha</p>
						</td>
					</tr>
					<tr>
						<td>
						<p>JUMLAH</p>
						</td>
						<td>
						<p>3485,74 ha</p>
						</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
	</tbody>
</table>

<table border="0" style="width:501px">
	<tbody>
		<tr>
			<td>
			<table align="center" border="0" style="width:289px">
				<tbody>
					<tr>
						<td colspan="3">
						<p><strong>Batas Wilayah Kecamatan Sukodono</strong></p>
						</td>
					</tr>
					<tr>
						<td>
						<p>Sebelah Utara &nbsp;</p>
						</td>
						<td>Kecamatan &nbsp;Taman</td>
					</tr>
					<tr>
						<td>Sebelah Selatan</td>
						<td>Kecamatan &nbsp;Sidoarjo</td>
					</tr>
					<tr>
						<td>
						<p>Sebelah Timur</p>
						</td>
						<td>Kecamatan Buduran</td>
					</tr>
					<tr>
						<td>Sebelah Barat &nbsp;</td>
						<td>Kecamatan &nbsp;Krian</td>
					</tr>
				</tbody>
			</table>
			</td>
		</tr>
	</tbody>
</table>
		</div>