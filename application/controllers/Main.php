<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {

	public function __construct(){
    parent::__construct();
    $this->load->model('blog_model');
    $this->load->library('template','upload');
  }

	public function index()
	{
		 $x['data']=$this->blog_model->get_all_post();
		 $this->template->utama('vmain',$x);
		
	}
}
